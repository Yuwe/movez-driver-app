package com.movez.movezdriver;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void gotoEmailLogin(View view){
        Intent intent = new Intent(this, EmailLoginActivity.class);
        startActivity(intent);
    }

    public void gotoMobileLogin(View view){
        Intent intent = new Intent(this, MobileLoginActivity.class);
        startActivity(intent);
    }

    public void gotoRegistration(View view){
        Intent intent = new Intent(this, RegAccountDetailsActivity.class);
        startActivity(intent);
    }
}
