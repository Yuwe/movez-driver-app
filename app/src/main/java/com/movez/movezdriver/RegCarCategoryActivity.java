package com.movez.movezdriver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class RegCarCategoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_car_category);
    }

    public void goBack(View view){
        Intent intent = new Intent(this, RegAccountDetailsActivity.class);
        startActivity(intent);
    }
}
