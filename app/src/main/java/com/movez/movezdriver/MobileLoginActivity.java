package com.movez.movezdriver;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MobileLoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_login);
    }

    public void gotoRegistration(View view){
        Intent intent = new Intent(this, RegAccountDetailsActivity.class);
        startActivity(intent);
    }
}
